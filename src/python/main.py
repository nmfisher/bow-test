import sys, os
from collections import Counter, defaultdict
from itertools import groupby, combinations
from operator import itemgetter
import random
from nltk.tokenize import sent_tokenize, word_tokenize
import re
import hashlib
import math

src_dir = "../../data"        
strip_regex = re.compile("<[\#a-zA-Z0-9\-!;\.%?&\+:= '\"\/_]+>")
TOKEN_WINDOW = 7

def get_filepaths(dir):
    filepaths = []
    for root, dirs, files in os.walk(dir):
        for name in files:
            filepaths.append(os.path.join(root, name))
    return filepaths;
    
def tokenize(filepath):
    with open(filepath, 'r') as file:
        raw = file.read().replace('\n', '')
    stripped = strip_regex.sub(" ", raw)
    tokenized = sent_tokenize(stripped)
    sentences = []
    for sentence in tokenized:
        sentences.append(word_tokenize(sentence));
    return sentences;

def split_and_hash(sentence, lambda_, token_window):
    for i in range(math.ceil(len(sentence) / token_window)):   
        if (i+1) * token_window > len(sentence):
            word_list = sentence[i:-1]
        else:
            word_list = sentence[i:(i+1)*token_window]
        word_set = frozenset(word_list)
        hash_ = hash(word_set)
        lambda_(hash_, word_list)

def tokenize_and_hash(filepath, lambda_, token_window):            
    sentences = tokenize(filepath);
    for sentence in sentences:
        split_and_hash(sentence, lambda_, token_window)            
        
def main():

    matches = defaultdict(list)
    
    def add_to_dict(hash_, word_list):
        if filepath not in matches[hash_]:
            matches[hash_].append(filepath);
    
    for filepath in get_filepaths(src_dir):
        tokenize_and_hash(filepath, add_to_dict, TOKEN_WINDOW)
                
    def hash_string(other_hash, word_list):
        if other_hash == hash_:
            as_string = " ".join([w for w in word_list if w not in [";",",","."]])
            strings[as_string].append(word_list)
    
    # iterate through all the hashes 
    for hash_ in matches:
        strings = defaultdict(list)
        # if the hash was found in more than one document 
        if len(matches[hash_]) > 1:
            # re-tokenize the document and find the word sequence that matches the hash
            for filepath in matches[hash_]:
                # stringify the word sequence, hash it and add to a lookup
                tokenize_and_hash(filepath, hash_string, TOKEN_WINDOW)
        
        # if the string-hash lookup only contains one key, the hashes matched because the string was identical (which we don't care about)
        if len(strings) > 1:
            for s in [string for string in strings if "[Home][Databases][WorldLII]" not in string]:
                print(s)
            print("##########")

    
    
if __name__ == '__main__':
  main()

import sys, os
from collections import Counter, defaultdict
from itertools import groupby, combinations
from operator import itemgetter
import random
import main as m
import re
import hashlib
import math
import matplotlib.pyplot as plt

NUM_DOCUMENTS = 100

def main():

    word_freqs = defaultdict(int)
    bigram_freqs = defaultdict(int)
    num_words = 0
    
    x1_ = []
    x2_ = []
    y1_ = []
    y2_ = []
    
        
    # this function is called for every bigram in every sentence
    # convert the bigram to an ordered tuple, hash it, and increment the counter for the hash
    # do the same for each individual word in the bigram
    # plot the total number of unique bigram/word hashes against the total number of words seen
    # this will show the number of unique bigrams growing faster than the number of unique words
    # we use hashes rather than the underlying tuple/string to avoid memory exhaustion
    # dealing with hash collisions is unnecessary for the purposes of the exercise
    def add_to_dict(hash_, word_list):
        nonlocal num_words
        bigram_freqs[hash(tuple(word_list))] += 1;
        for word in word_list:
            num_words += 1
            word_freqs[hash(word)] += 1
            x1_.append(num_words)
            y1_.append(len(word_freqs))
        x2_.append(num_words)
        y2_.append(len(bigram_freqs))
    
    count = 0
    for filepath in m.get_filepaths(m.src_dir):
        if count < NUM_DOCUMENTS:
            m.tokenize_and_hash(filepath, add_to_dict, 2)
        else:
            break;
        count += 1;
            
    wf = plt.scatter(x1_, y1_)
    bfg = plt.scatter(x2_, y2_)
    plt.xlabel('Total words')
    plt.ylabel('Unique')
    plt.legend((wf, bfg), ('Words','Bigrams'))
    plt.tight_layout(pad=0.9)
    plt.savefig("../../output/plot.png")
    plt.clf()
    
    
if __name__ == '__main__':
  main()

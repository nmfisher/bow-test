#/usr/bin/perl -w
use strict;
use warnings;
use threads;
require "./wget.pl";

my $base_dir = "../../data";

my $base_url = "http://www.austlii.edu.au/au/cases/sa/SASC";
my @years = (1991..2016);
my @case_urls;

foreach my $year (@years) {
    my $sub_dir = $base_dir . "/$year";
    if(!-d $sub_dir) {
        unless(mkdir $sub_dir) {
            die "Unable to create directory $year\n";
        }
    }
    open (CMDOUT, wget("$base_url/$year",  "-O-"));
    while (my $line = <CMDOUT>) {
        if($line =~ /\.\.(\/$year\/([0-9]+.html))/) {
            push @case_urls, $1;
        }
    }
}

foreach my $case_url (@case_urls) {
    my $year = $case_url;
    $year =~ /([0-9]{4})/;
    open (CMDOUT, wget($base_url . $case_url,  "--directory-prefix $base_dir/$1"));
    while (my $line = <CMDOUT>) {
        print $line;
    }
}

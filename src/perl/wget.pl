my $useragent = "\"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36\"";
my $cookie = "\"\"";

sub wget {
    my $target = $_[0];
    my $flags = $_[1] ? $_[1] : "";
    return "wget --user-agent $useragent --header $cookie $flags $target 2>&1 |";
}

sub get {
    open (CMDOUT, wget($_[0],  "-O-"));
    my $output = "";
    while (my $line = <CMDOUT>) {
        $output = $output . $line;
    }   
    return $output;
}
